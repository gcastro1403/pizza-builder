
public class Pizza {
	
		private final String sauce;
		private final String cheese;
		private final String base;
		private final String meat;
		
		private Pizza(PizzaBuilder pizzaBuilder) {
			this.sauce = pizzaBuilder.sauce;
			this.cheese = pizzaBuilder.cheese;
			this.base = pizzaBuilder.base;
			this.meat = pizzaBuilder.meat;
		}
		
		
		
		public String getSauce() {
			return sauce;
		}



		public String getCheese() {
			return cheese;
		}



		public String getBase() {
			return base;
		}
		
		

		public String getMeat() {
			return meat;
		}
		
		

		@Override
		public String toString() {
			return "Pizza [sauce=" + sauce + ", cheese=" + cheese + ", base=" + base + ", meat=" + meat + "]";
		}



		public static class PizzaBuilder{
			private  String sauce;
			private  String cheese;
			private  String base;
			private  String meat;
			
			
			PizzaBuilder(String sauce, String cheese, String base){
				this.sauce = sauce;
				this.cheese = cheese;
				this.base = base;
			}
			
			public PizzaBuilder sauce (String sauce) {
				this.sauce = sauce;
				return this;
			}

			
			public PizzaBuilder cheese (String cheese) {
				this.cheese = cheese;
				return this;
			}
			
			
			public PizzaBuilder base (String base) {
				this.base = base;
				return this;
			}
			
			public PizzaBuilder meat (String meat) {
				this.meat = meat;
				return this;
			}
			
			
			public Pizza bake () {
				
				return new Pizza( this );
			}
		}

		
}


